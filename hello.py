from flask import Flask
from flask import request
from flask import make_response
from flask import redirect
from flask import render_template
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)
moment = Moment(app)

@app.route('/')
def homepage():
    response = make_response('<h1>This document carries a cookie!</h1>')
    a=response.headers
    b=request.headers
    response.set_cookie('answer', '12')
    return "{}<hr>{}".format(a,b)

@app.route('/presentation')
def presentation():
    #return redirect("https://www.groupeism.sn/")
    return render_template("presentation/presentation.html")

@app.route('/profile/<nom>')
def profile(nom):
    return "<h2>Bienvenue M. {} </h2>".format(nom)


@app.route('/welcome/<etudiant>')
def welcome(etudiant):
    info = dict()
    info['etudiant']=etudiant
    info['ecole']=request.args.get('school')
    info['diplome']=request.args.get('diploma')
    info['niveau']=int(request.args.get('level'))
    
    return render_template("presentation/welcome.html", info=info)

